const { client } = require('nightwatch-cucumber');
const { Given, Then, When } = require('cucumber');
const signup = client.page.signup();
Given(/^I am on the user signup page$/, () => {
	return signup.isBodyDisplayed();
});

