module.exports = {

    url: function () {
      return this.api.launchUrl.concat('/login');
    },
  
    elements: {
      mainHeading: 'h1',
      
    },
  
    commands: [
      {
        isBodyDisplayed: function (element) {
          return this.navigate()
            .waitForElementVisible('body', 2000);
        },
        
      }]
  }
  